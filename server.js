// Works with Api get 

// Get dependencies
const express = require('express');
const app = express();

const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');
var axios = require('axios');
var cors = require('cors');
// Get our API routes

const alert = require('./server/routes/alert');
const jiraForest = require('./server/routes/jira-forest');
const jiraParameters = require('./server/routes/jiraParameters');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Cors middleware
var allowCrossDomain = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET, POST, DELETE, PUT, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, X-Codingpedia,Authorization, Accept');
    next();
}
app.use(allowCrossDomain);


// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', alert);
app.use('/api', jiraForest);
app.use('/api', jiraParameters);


// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html')); // 'dist/index.html'));
});


const port = process.env.PORT || '3000';
app.listen(port, function() {
    console.log("Server started on port: " + port);
});