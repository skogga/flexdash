const express = require('express');
const router = express.Router();
var mongojs = require('mongojs');

router.get('/alert', function(req, res, next) {
    // Retrieve
    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/';
    // Connect to the db
    MongoClient.connect(url, function(err, db) {
        if (!err) {
            const dataBase = db.db('flexDB');
            var collection = dataBase.collection('alerts');
            collection.find().toArray(function(err, docs) {
                assert.equal(null, err);

                res.json(docs);
            });
        }
    });
});


router.post('/newAlert', function(req, res, next) {

    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/';

    var alert = req.body;
    var newAlert = {};

    if (!alert.publishDate || !alert.expiryDate || !alert.title || !alert.message || !alert.priority) {
        console.log("error in alert.js");
        res.json({
            "error": "Bad data in new alert"
        });
    } else {
        MongoClient.connect(url, function(err, db) {
            if (!err) {
                const dataBase = db.db('flexDB');
                var collection = dataBase.collection('alerts');

                try {
                    collection.insertOne(alert);
                } catch (e) {
                    print(e);
                };
            }
        });
    }
});

// Update alert
router.put('/updateAlert/:id', function(req, res, next) {

    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/';
    // Connect to the db
    var alert = req.body;

    var updatedAlert = {};

    if (alert.title) {
        updatedAlert.title = alert.title;
    }
    if (alert.message) {
        updatedAlert.message = alert.message;
    }
    if (alert.priority) {
        updatedAlert.priority = alert.priority;
    }
    if (alert.publishDate) {
        updatedAlert.publishDate = alert.publishDate;
    }
    if (alert.expiryDate) {
        updatedAlert.expiryDate = alert.expiryDate;
    }

    if (!updatedAlert) {
        res.json({
            "error": "Bad data"
        });
    } else {

        MongoClient.connect(url, function(err, db) {
            if (!err) {
                const dataBase = db.db('flexDB');
                var collection = dataBase.collection('alerts');


                collection.update({ _id: mongojs.ObjectId(req.params.id) }, updatedAlert, {}, function(err, alert) {
                    assert.equal(null, err);

                    if (err) {
                        console.log("error in alerts.js (update)");
                    }
                });

            } else {
                console.log('error in db connection (alerts.js update)');
                console.log(err);
            }
        });
    }
});


router.post('/removeAlert/:id', function(req, res, next) {

    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/';

    var alert = req.body;

    console.log('in alerts.js remove alert');

    if (!alert.publishDate || !alert.expiryDate || !alert.title || !alert.message || !alert.priority) {
        console.log("error in alert.js");
        res.json({
            "error": "Bad data in remove alert"
        });
    } else {
        MongoClient.connect(url, function(err, db) {
            if (!err) {
                const dataBase = db.db('flexDB');
                var collection = dataBase.collection('alerts');

                collection.remove({ _id: mongojs.ObjectId(req.params.id) }, function(err, alert) {
                    //assert.equal(null, err);
                    if (err) {
                        console.log("error in alerts.js (remove)");
                    } else {
                        res.send((alert === 1) ? { msg: 'Deleted' } : { msg: 'error: ' + err });
                    }

                });
            }
        });
    }
});

module.exports = router;