const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');

var ts;

var structureId;
var structureFolderRegEx; 
var field;

router.post('/jiraForest', function(req, res, next) {

    var entry = req.body;
   /*  console.log('req.body');
    console.log(req.body); */
    fields = req.body.fields;
    structureFolderRegEx = new RegExp(req.body.structureFolderRegex);
    structureId = req.body.structureID;

    var options = {
        url: "https://jira.scania.com/rest/structure/2.0/forest/latest?s={%22structureId%22:" + structureId + "}",
        method: 'GET',
        json: true,
        headers: {
            Authorization: 'Basic XXX'
        }
    };

    var rp = require('request-promise');
    ts = new Date().getTime();
    console.log('Running first query (get structure from one ID)');
    rp(options)
        .then(function(body) {
            console.log('Took ' + (new Date().getTime() - ts) + ' ms');

            const formulas = body.formula.split(',');

            const rows = [];
            for (let i = 0; i < formulas.length; i++) {

                const obj = {
                    rowId: formulas[i].substring(0, formulas[i].indexOf(':')),
                    rowDepth: formulas[i].substring(formulas[i].indexOf(":") + 1, formulas[i].lastIndexOf(":")),
                    itemId: formulas[i].substring(formulas[i].lastIndexOf(":") + 1, formulas[i].length)
                }
                rows.push(obj);
            }

            const rowIds = rows.map(a => a.rowId);
          
          
            const opts = {
                method: 'POST',
                uri: "https://jira.scania.com/rest/structure/2.0/value.json",

                json: true,
                headers: {
                    Authorization: 'Basic XXX'
                },
                data: {
                    "requests": [{
                        "forestSpec": {
                            "structureId": structureId

                        },
                        "rows": rowIds,
                        "attributes": [{
                                "id": "summary",
                                "format": "text"
                            },
                            {
                                "id": "key",
                                "format": "text" /* html */
                            },
                            {
                                "id": "progress",
                                "format": "number",
                                "params": {
                                    "basedOn": "timetracking",
                                    "resolvedComplete": true,
                                    "weightBy": "equal"
                                }
                            }
                        ]
                    }]
                }
            };

            var rp2 = require('request-promise');
            var options2 = {
                method: 'POST',
                uri: 'https://jira.scania.com/rest/structure/2.0/value',
                headers: {
                    Authorization: 'Basic XXX'
                },
                body: opts.data,
                json: true // Automatically stringifies the body to JSON
            };
            var entriesInStructure = [];
            ts = new Date().getTime();
            console.log('Running second query (get info from treestructure IDs)');
            rp2(options2)
                .then(function(body) {
                    
                    console.log('Took: ' + (new Date().getTime() - ts) + ' ms');

                    for (let i = 0; i < body.responses[0].data[0].values.length; i++) {

                        const entry = {
                            name: body.responses[0].data[0].values[i],
                            sort: body.responses[0].data[1].values[i],
                            levelId: parseInt(rows[i].rowDepth),
                            itemId: rows[i].itemId
                        }
                        entriesInStructure.push(entry);
                    }
                    
         
                    var okToPush = false;
                    var filteredArr = [];
                    entriesInStructure.forEach(el => {
                        if (el.levelId == 0 && structureFolderRegEx.test(el.name)) {
                            okToPush = true;
                        }
                        else if (el.levelId == 0 && !structureFolderRegEx.test(el.name)) {
                            okToPush = false;
                         }
                         if (okToPush) {
                            filteredArr.push(el);
                         }
                    });
                    const treesWithIssues = filteredArr.filter(tree => tree.sort);
                    
                    forestKeys = [];

                    treesWithIssues.forEach(function(element) {
                        forestKeys.push(element.sort);
                    });

                    
                   /*  &fields=' + joinedFields */
                    var joinedFields = fields.join();
                    var rp3 = require('request-promise');
                    var options3 = {
                        method: 'GET',
                        uri: 'https://jira.scania.com/rest/api/2/search?expand=changelog&jql=key in (' + forestKeys + ')&maxResults=200',
                        headers: {
                            Authorization: 'Basic XXX'
                        },
                        json: true
                    };
                    ts = new Date().getTime();
                    console.log('Running third query (getting ' + forestKeys.length + ' issues with info)');
                    rp3(options3)
                        .then(function(body) {
                        
                            console.log('Took: ' + (new Date().getTime() - ts) + ' ms');

                            var fields = body.issues;
                   
                            var treesWithFields = entriesInStructure.map(function(entry) {
                                if (fields.findIndex(x => x.key == entry.sort) != -1) {
                                    var el = fields[fields.findIndex(x => x.key == entry.sort)];
                                    entry.fields = el.fields;
                                    entry.changelog = el.changelog;
                                }
                                return entry;
                            });

                            var result = [],
                                levels = [result];
                            treesWithFields.forEach(({ levelId, name, sort, fields, itemId, changelog }) =>
                                levels[levelId].push({ levelId, name, sort, fields, itemId, changelog, children: levels[levelId + 1] = [] })
                            );
                          
      
                            res.send(result);

                        })
                        .catch(function(err) {

                            console.log('error in rp3 (get forest) ');
                            console.log(err);
                        });

                })
                .catch(function(err) {
                    console.log('error in rp2 (get forest) ');
                    console.log(err);
                });
        })
        .catch(function(err) {
            console.log('error in rp (get forest)');
            console.log(err);
        });
});


module.exports = router;













/* 371 */
/* new RegExp("^(20[0-9][0-9]) [Qq].*[0-9]"); */
/* [
'priority',
'fixVersions',
'lastViewed',
'priority',
'labels',
'aggregatetimeoriginalestimate',
'timeestimate',
'versions',
'issuelinks',
'assignee',
'status',
'components',
'aggregatetimeestimate',
'creator',
'subtasks',
'reporter',
'aggregateprogress',
'progress',
'votes',
'issuetype',
'timespent',
'project',
'aggregatetimespent',
'project',
'aggregatetimespent',
'resolutiondate',
'workratio',
'watches',
'created',
'updated',
'timeoriginalestimate',
'description',
'summary',
'environment',
'duedate'
]; */