import { TestBed, inject } from '@angular/core/testing';

import { JiraParametersService } from './jira-parameters.service';

describe('JiraParametersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraParametersService]
    });
  });

  it('should be created', inject([JiraParametersService], (service: JiraParametersService) => {
    expect(service).toBeTruthy();
  }));
});
