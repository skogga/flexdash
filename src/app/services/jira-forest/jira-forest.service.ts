import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class JiraForestService {

  constructor(private http: Http) { }

  getJiraForest(params) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/jiraForest/', 
        JSON.stringify(params),
        {headers: headers})
            .map(res => res.json());
  }

  /* getJiraFormulaInfo() {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/jiraFormulaInfo/',
        {headers: headers})
            .map(res => res.json());
  } */
}
