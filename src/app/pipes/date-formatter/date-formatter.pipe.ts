import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormatter'
})
export class DateFormatterPipe implements PipeTransform {

   ordinal_suffix_of(i) {
    const j = i % 10,
        k = i % 100;
    if (j === 1 && k !== 11) {
        return i + 'st';
    }
    if (j === 2 && k !== 12) {
        return i + 'nd';
    }
    if (j === 3 && k !== 13) {
        return i + 'rd';
    }
    return i + 'th';
  }
   transform(value: string, dateOrTime: string): any {
    if (!value) {
        return value;
    }


    const inputDate = new Date(value);

    if (dateOrTime.toLowerCase() === 'date') {
      const year = inputDate.getFullYear().toString();

          // let month =// inputDate.getMonth(); // + 1).toString()
          /* if (month.length < 2) {
            month = '0' + month;
          } */

          const month = inputDate.toLocaleString('en-us', { month: 'short' });

          let date = inputDate.getDate().toString();
          /*if (date.length < 2) {
            date = '0' + date;
          }*/
          date = this.ordinal_suffix_of(date);

          const dateVersion = date + ' ' + month + ' - ' + year;
          return dateVersion;
    }
    else if (dateOrTime.toLowerCase() === 'time') {
      let hour = inputDate.getHours().toString();
      if (hour.length < 2) {
        hour = '0' + hour;
      }

      let minutes = inputDate.getMinutes().toString();
      if (minutes.length < 2) {
        minutes = '0' + minutes;
      }

      let seconds = inputDate.getSeconds().toString();
      if (seconds.length < 2) {
        seconds = '0' + seconds;
      }
      const timeVersion = hour + ':' + minutes; // + ':' + seconds;
      return timeVersion;
    }
  }

}
