import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'portfolioFilter'
})
export class PortfolioFilterPipe implements PipeTransform {

  transform(items: any[], chosenFilters: any[]): any {
 
    if (!items || !chosenFilters || chosenFilters.length == 0) {
      return items;
    }

    var resArr = [];
   
    

    if (chosenFilters.find(o => o.name === 'status') && chosenFilters.find(o => o.name === 'status').types.length > 0) {
      items.forEach(element => {
        // om de valda statusarna inkluderar itemets status
      
        if (element.fields.status.name && chosenFilters.find(o => o.name === 'status').types.includes(element.fields.status.name)){
          resArr.push(element);
        }
        // om är en folder
        if(!element.sort) {
          resArr.push(element);
        }
      });
      return resArr;
      /* return chosenFilters.find(o => o.name === 'status')
      return items.filter(item => item.status.name.indexOf(chosenFilters.value) !== -1); */
    }
    else {
      return items;
    }

   /*  chosenFilters.find(o => o.name === 'status') */
    
    // return items.filter(item => item.fields.status.name.indexOf(chosenFilters.value) !== -1);
   // return items;
  }

}
