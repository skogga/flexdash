import { Pipe, PipeTransform } from '@angular/core';
import {FormControl} from '@angular/forms';
@Pipe({
  name: 'issueFilter',
  pure: false
})
export class IssueFilterPipe implements PipeTransform {

  transform(items: any[], chosenFilters: FormControl, allFilters: any[]): any {
 
    if (!items || !chosenFilters.value || chosenFilters.value.length === 0) {
      return items;
    }
/* 
    console.log('chosen filters?');
    console.log(chosenFilters.value); */
   /*  chosenFilters.value.forEach(element => {
      console.log('chosen filter: ' + element);
    }); */
    
    // return items.filter(item => item.fields.status.name.indexOf(chosenFilters.value) !== -1);
    return items;
  }

}
