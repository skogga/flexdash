import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chosenApps'
})
export class ChosenAppsPipe implements PipeTransform {

  transform(items: any[], chosenApps: any[]): any {
    if (!items || !chosenApps || chosenApps.length === 0) {
      return items;
    }

    else {
      return items.filter(i => (i.fields && !chosenApps.includes(i.fields.project.key)) || i.fields.project.key == 'CBM');
    }
    

  }

}
