import { HostListener, Component, Input, OnInit, OnDestroy } from '@angular/core';
declare var $: any;
import { NgbPopoverModule, NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';
import { FilterService } from '../../services/filter/filter.service';
import { InlineSVGModule } from 'ng-inline-svg';
import { SVGCacheService } from 'ng-inline-svg';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'forest-item',
  templateUrl: './forest-item.component.html',
  styleUrls: ['./forest-item.component.css']
})
export class ForestItemComponent implements OnInit, OnDestroy {
    private myImage: SafeHtml;
    filters: any;
    subscriptionFilters: Subscription;

    constructor(private filterService: FilterService, private sanitizer: DomSanitizer) { 
    //    this.myImage = sanitizer.bypassSecurityTrustHtml(product.StructureSVG);
        this.subscriptionFilters = this.filterService.getFilters().subscribe(res =>{
            this.filters = res;
        })
    }
    

    filterControl = new FormControl();
    
    @Input()
    tree;

    @Input()
    chosenFilters;

    runningLocally: any;
    lastPopoverRef: any;

    @HostListener('document:click', ['$event'])
    clickOutside(event) {
      // If there's a last element-reference AND the click-event target is outside this element
      if (this.lastPopoverRef && !this.lastPopoverRef._elementRef.nativeElement.contains(event.target)) {
        this.lastPopoverRef.close();
        this.lastPopoverRef = null;
      }
    }
      
    setCurrentPopoverOpen(popReference) {
        // If there's a last element-reference AND the new reference is different
        if (this.lastPopoverRef && this.lastPopoverRef !== popReference) {
            this.lastPopoverRef.close();
        }
        // Registering new popover ref
        this.lastPopoverRef = popReference; 
    }
    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscriptionFilters.unsubscribe();
    }
    ngOnInit() {
  
        if (document.location.origin.includes('localhost')) {
            this.runningLocally = true;
        }
        else {
            false;
        }
   
        this.accordionSetup();
    
        /* 
        setInterval(() => {
            console.log('filters in forest-item:');
            console.log(this.chosenFilters);
        }, 2000); 
        */
    }
   /*  doneRepeating(i, l) {
        console.log('i: ' + i + ', l: ' + l);
        console.log('tree done?');
    } */
    getRelevantEntries(arr) {
        const fields = ['status', 'Fix Version', 'Component', 'assignee', 'priority', 'reporter']; // , 'Rank', 'Link', 'resolution'
        return arr.filter(e => e.items.some(i => fields.includes(i.field)));
    }

    
    fixVersionIsARelease(name) {
        var reg = new RegExp("^([mM][aA][cC]|[mM][eE][mM]|[mM][pP][pP])( )*(201[0-9])(\.| )?([0-9][0-9]?)(\.| )?([0-9][0-9]?)$");
        return reg.test(name);
    }
    
    accordionSetup() {
    
     
        $('.accordion h1, h2, h3, h4, h5, h6').unbind().click(function(e) {
            
            const target = e.target,
                name = target.nodeName.toUpperCase();

            const kids = target.parentNode.childNodes;
            console.log();
            if ($(target).hasClass('open')) {
                console.log('had open');
                $(target).removeClass('open');
                $(target).parent().parent().next().children('.holder').removeClass('open'); 

            if ($(target).parent().next('.nokids')) {
                    $(target).parent().next('.nokids').removeClass('open');
                }
            } else {
                console.log('did not have open');
                $(target).addClass('open');
                $(target).parent().parent().next().children('.holder').addClass('open');

                if ($(target).parent().next('.nokids')) {
                    $(target).parent().next('.nokids').addClass('open');
                }
                console.log($(target).next().children().has('.holder').length);
            }
        });
  }

}




