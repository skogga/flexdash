import { Component, ViewChild, OnInit, HostListener, ElementRef,AfterViewInit } from '@angular/core';

import { ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
declare var $: any;

/* import { JiraForestComponent } from '../jira-forest/jira-forest.component'; */

import { VariablePasserService } from '../../services/variable-passer/variable-passer.service';



@Component({
  selector: 'flex-wrap-component',
  templateUrl: './flex-wrap.component.html',
  styleUrls: ['./flex-wrap.component.css']
})
export class FlexWrapComponent implements AfterViewInit {
  // Starta med en box, måste vara en array för att köra ngFor, så pusha/popa med ettor bara.
  /* @ViewChild(JiraForestComponent) JF: JiraForestComponent;  */
  

  boxes = [];
  myData: any;

  constructor (private sharedService: VariablePasserService) {
    this.sharedService.data.subscribe(
      (data: any) => {
        console.log('subscribed value in flex-wrap');
        console.log(data);
        this.myData = data;
    });
  }

  @HostListener('window:resize', ['$event'])
  sizeChange(event) {
    this.resizeBoxes();
  }

  ngAfterViewInit() {
    /* console.log('in flex-wrap, title of jira forest: ' + this.JF.returnTitle()); */
    for (let i = 0; i < 6; i++) {
      this.boxes.push(this.randObj());
    }
  }

  addRemoveFilter($event) {
    console.log($event);
  }

  doneRepeating() {
    console.log('done repeating');
    this.resizeBoxes();
  }

  resizeBoxes() {

    const boxes = $('.flex .box');
    let cols = 0;

    const firstRowHeight = $(boxes[0]).offset().top;

    for (let i = 0; i < boxes.length; i++) {
      if ($(boxes[i]).offset().top === firstRowHeight) {
        cols += 1;
      }
      else {
        break;
      }
    }
    document.documentElement.style.setProperty(`--colsPerRow`, 'calc(100% / ' + cols + ')');
  }

  randObj() {
    const randNr = Math.floor(Math.random() * 100) + 1;
    const randObj = {name: 'Box' + randNr};
    return randObj;
  }

  addBox() {
    this.boxes.push(this.randObj());
    this.resizeBoxes();
  }

  removeBox(index) {

    if (index) {
      $('.flex .box').eq(index).remove();
      const a =  this.boxes.findIndex(x => x.name.toLowerCase() === index.toLowerCase());
      this.boxes.splice(a, 1);
    }

    else {
      this.boxes.pop();
    }
    this.resizeBoxes();
  }


  resizeBox(boxIndex) {

    const activeBox = $('.box').eq(boxIndex);
    if (!activeBox.hasClass('boxFull')) {
      activeBox.addClass('boxFull');
    }
    else {
      activeBox.removeClass('boxFull');
    }
  }

}
