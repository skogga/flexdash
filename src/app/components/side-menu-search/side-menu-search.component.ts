import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'side-menu-search',
  templateUrl: './side-menu-search.component.html',
  styleUrls: ['./side-menu-search.component.css']
})
export class SideMenuSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  playAnim(type){
    console.log('in playAnim side-menu.comp');
    if (type === 'show') {
      $('.sideContent').addClass('active');
    }
    else {
      $('.sideContent').removeClass('active');
    }
  }
  closeSideMenu(id) {
    document.getElementById(id).style.width = '0';  
  }
}
