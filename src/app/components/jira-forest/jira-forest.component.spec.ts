import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JiraForestComponent } from './jira-forest.component';

describe('JiraForestComponent', () => {
  let component: JiraForestComponent;
  let fixture: ComponentFixture<JiraForestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JiraForestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JiraForestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
